#define While 257
#define Skip 258
#define If 259
#define Then 260
#define Else 261
#define Do 262
#define Od 263
#define Assign 264
#define And 265
#define Declare 266
#define Begin 267
#define End 268
#define Integer 269
#define Identifier 270
#define Call 271
#define Proc 272
#define Is 273
#define Var 274
#define Leq 275
#define Geq 276
#define Lexical_error 277
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union{
  PTR_NODE u_node; 
  char u_char[120];
  int u_int ;
} YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
extern YYSTYPE yylval;
