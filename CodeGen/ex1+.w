
declare
	
    var a;
    var b;
	var c;
    var d;
    var x
begin
	a := 1;
    b := 4;
    c := b + 2;
    d := a + b;
    if (b+2 = c) then x := 8 else x := 10;
    while (x < 20) do
        x := x + 1
    od
end
