#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "type.h"

int * search(char *str);

typedef struct symbol //symbol table struct (list)
{
    char* name;
    int val;
    char status;
    struct symbol* next;
}symbol;

symbol* symbolTable = NULL;
symbol* symbolCurr = NULL;

typedef struct proc //proc table struct (list)
{
  char* name;
  NODE* root;
  symbol* localState;
  struct proc* next;
}proc;

proc* procTable = NULL;
proc* procCurr = NULL;

typedef struct state
{
  symbol* state;
  struct state* next;
}state;

state* stateStack = NULL;

NODE *mk_node(type,n1,n2)
int type;
NODE *n1;
NODE *n2;
{
  NODE *n;
  n = (NODE *) calloc (1,sizeof(NODE));
  n->type_node=type;
  n->fg=n1;
  n->fd=n2;
  return(n);
}

NODE *mk_leaf_int(val)
int val;
{
  NODE *n;
  n = (NODE *) calloc (1,sizeof(NODE));
  n->type_node=NUM;
  (n->val_node).u_int=val;
  return(n);
}

NODE *maj_leaf_str(val)
char * val;
{
  NODE *n;
  int i ;
  n = (NODE *) calloc (1,sizeof(NODE));
  n->type_node=IDF;
  strcpy((n->val_node).u_str, val) ;
  return(n);
}

NODE *mk_leaf_str(val)
char * val;
{
  NODE *n;
  int i ;
  n = (NODE *) calloc (1,sizeof(NODE));
  n->type_node=IDF;
  strcpy((n->val_node).u_str, val) ;
  return(n);
}

print_sep()
{
  printf(" ");
}


//Symbol table manipulators
int isInTable(char* name) //checks if a variable has already been assigned
{
  symbol* S = symbolTable;
  while(S)
  {
    if(strcmp(S->name,name) == 0)
      return 1;
    S = S->next;
  }
  return 0;
}

void Reassign(char* name, int val)
{
  symbol* S = symbolTable;
  while( (S!=NULL) && (strcmp(S->name, name)!=0))
    S = S->next;
  S->val = val;
}

void Assign(char* name, int val)
{
  if(symbolCurr == NULL){
    symbolCurr = malloc(sizeof(symbol));
    symbolTable = symbolCurr;
  }
  else
  {
    symbolCurr->next = malloc(sizeof(symbol));
    symbolCurr = symbolCurr->next;
  }
  symbolCurr->name = name;
  symbolCurr->val = val;
}

int GetValue(char* name)
{
  symbol* S = symbolTable;
  while(S && (strcmp(S->name, name)!=0))
    S = S->next;
  return S->val;
}

//-----------------------------------------------
//proc table manipulators
int procIsInTable(char* name) //checks if a variable has already been assigned
{
  proc* P = procTable;
  while(P)
  {
    if(strcmp(P->name,name) == 0)
      return 1;
    P = P->next;
  }
  return 0;
}

void procReassign(char* name, NODE* root, symbol* state)
{
  proc* P = procTable;
  while( (P!=NULL) && (strcmp(P->name, name)!=0))
    P = P->next;
  P->root = root;
  P->localState = state;
}

void procAssign(char* name, NODE* root, symbol* state)
{
  if(procCurr == NULL){
    procCurr = malloc(sizeof(proc));
    procTable = procCurr;
  }
  else
  {
    procCurr->next = malloc(sizeof(proc));
    procCurr = procCurr->next;
  }
  procCurr->name = name;
  procCurr->root = root;
  procCurr->localState = state;
}

void pushState() //saves current state in the stack
{
  state* S = stateStack;
  while(S)
    S = S->next;
  S = malloc(sizeof(state));
  S->state = symbolTable;
}

void pullState() //restores last state on the stack
{
  state* S = stateStack;
  while(S->next)
    S->next;
  symbolTable = S->state;
}

//-----------------------------------------------

print_node(n)
NODE *n;
{
  if (n) {
  switch(n->type_node)
  {
  	case SKIP:
	  print_sep();
   	  printf("Skip");
  	  print_sep();
  	  break;
    case COMMA:
	  print_sep();
  	  print_node(n->fg);
	  printf(", ");
	  print_sep();
  	  print_node(n->fd);
  	  break;
    case SEMI_COLON:
	  print_sep();
	  print_sep();
  	print_node(n->fg);
	  printf(" ;\n");
	  print_sep();
	  print_sep();
  	  print_node(n->fd);
  	  break;
    case WHILE:
	  printf("\nWhile\n") ;
	  printf("(");
  	  print_node(n->fg);
	  printf(")");
	  printf("\nDo\n") ;
  	  print_node(n->fd);
	  printf("\nOd\n") ;
  	  break;
    case IF:
	  printf("\nIf ") ;
  	  print_node(n->fg);
  	  print_node(n->fd);
  	  break;
    case THENELSE:
	  printf(" Then ") ;
  	  print_node(n->fg);
	  printf(" Else ") ;
  	  print_node(n->fd);
  	  break;
    case BLOC:
  	  print_node(n->fg);
      printf("\nBegin\n");
  	  print_node(n->fd);
      printf("\nEnd\n");
  	  break;
    case ASSIGN:
      print_node(n->fg);
	  printf(":=") ;
  	  print_node(n->fd);
  	  break;
    case PLUS:
  	  print_node(n->fg);
	  print_sep();
	  printf(" + ");
	  print_sep();
  	  print_node(n->fd);
  	  break;
    case AND:
  	  print_node(n->fg);
	  print_sep();
	  printf(" and ");
	  print_sep();
  	  print_node(n->fd);
  	  break;
    case SUP:
  	  print_node(n->fg);
	  print_sep();
	  printf(" > ");
	  print_sep();
  	  print_node(n->fd);
  	  break;
  	case SUPEQ:
  	  print_node(n->fg);
	  print_sep();
	  printf(" => ");
	  print_sep();
  	  print_node(n->fd);
  	  break;
  	 case INF:
  	  print_node(n->fg);
	  print_sep();
	  printf(" < ");
	  print_sep();
  	  print_node(n->fd);
  	  break;
  	case INFEQ:
  	  print_node(n->fg);
	  print_sep();
	  printf(" =< ");
	  print_sep();
  	  print_node(n->fd);
  	  break;
    case EGAL:
  	  print_node(n->fg);
	  print_sep();
	  printf(" = ");
	  print_sep();
  	  print_node(n->fd);
  	  break;
    case NOT:
	  printf("^");
	  printf("(");
  	  print_node(n->fg);
	  printf(")");
	  print_sep();
  	  break;
    case TIMES:
  	  print_node(n->fg);
	  print_sep();
	  printf(" * ");
	  print_sep();
  	  print_node(n->fd);
  	  break;
    case NUM:
  	  printf("%d",(n->val_node).u_int);
  	  break;
    case IDF:
  	  printf("%s", (n->val_node).u_str);
  	  break;
    case CALL:
	  printf("Call ");
  	  print_node(n->fg);
	  printf("(");
	  print_node(n->fd);
	  printf(")");
	  break ;
    case PROC_DECL:
	  printf("\nProc") ;
	  print_sep();
  	  print_node(n->fg);
  	  printf("(");
  	  print_node(n->fd->fg);
  	  printf(")\n");
  	  print_node(n->fd->fd);
	break;
  }
  }
}

void print_tree()
{
  print_node(root);
  printf("\n");
}

int exec_node(n)
NODE *n;
{
  if (n) {
    switch(n->type_node)
    {
  	case SKIP:
  	  break;
    case COMMA: //TODO
      exec_node(n->fd);
  	  break;
    case SEMI_COLON:
      exec_node(n->fg);
      exec_node(n->fd);
  	  break;
    case WHILE: //LOW
      while( exec_node(n->fg))
      {
        exec_node(n->fd);
      }
  	break;
    case IF:
      if( exec_node(n->fg) )
      {
        exec_node( n->fd->fg );
      }
      else
      {
        exec_node( n->fd->fd );
      }
  	  break;
    case THENELSE: //MED
      printf("Never met THENELSE");
      exec_node(n->fd);
  	  break;
    case BLOC:
      exec_node(n->fg);
      exec_node(n->fd);
  	  break;
    case ASSIGN:
      if(isInTable(n->fg->val_node.u_str))
      {
        Reassign(n->fg->val_node.u_str,exec_node(n->fd));
      }
      else
      {
        printf("ERROR : Assigning undeclared variable\n");
        exit(0);
      }
  	  break;
    case PLUS:
        return exec_node(n->fg) + exec_node(n->fd);
  	break;
    case AND:
	return ( exec_node( n->fg ) && exec_node( n->fd ) ); 
  	  break;
    case SUP:
	return ( exec_node( n->fg ) > exec_node( n->fd ) );
  	  break;
  	case SUPEQ:
	return ( exec_node( n->fg ) >= exec_node( n->fd ) );
   	  break;
  	case INF:
	return ( exec_node( n->fg ) < exec_node( n->fd ) );
  	  break;
  	case INFEQ:
	return ( exec_node( n->fg ) <= exec_node( n->fd ) );
  	  break;
    case EGAL:
	return ( exec_node( n->fg ) == exec_node( n->fd ) );
  	  break;
    case NOT:
	return (! exec_node( n->fg ) );
  	  break;
    case TIMES:
     	  return exec_node(n->fg) * exec_node(n->fd); 
  	  break;
    case NUM:
      return n->val_node.u_int;
  	  break;
    case IDF:
      if(!(isInTable((n->val_node).u_str)))
        Assign((n->val_node).u_str, 999999); //TODO replace dummy value
      else
		  return GetValue( n->val_node.u_str);
      break;
    case CALL: //LOW
      /*
      push current state
      symboltableAdd(proc->state)
      exec_tree(proc->node)
      pull state
      */
      break;
    case PROC_DECL: //LOW
      /*
      Push current state
      Create the state for the procedure //need to keep all vars from the previous state and have a link to them in the new state so higher scope vars can be changed by the proc. Maybe a new func for that.
      Create procedure object in procTable
      pull state
      */
      break;
    }
  }
}

void print_state()
{
  symbol* S = symbolTable;
  printf("------------Symbols------------\n");
  while(S != NULL){
    printf("%s",S->name);
    printf("->");
    printf("%d",S->val);
    printf("\n");
    S = S->next;
  }
  printf("--------------End--------------\n");
}
int * search(char *str)
{
	symbol* S = symbolTable;
	while(S!=NULL)
	{
		if(!strcmp(str,S->name))
		{
			return (void*) &(S->val);
		}
		S= S->next;
	}
	return NULL;//if we can not find symbol there.
}

void exec_tree()
{
  exec_node(root);
  printf("\n");
}
