#ifndef lint
static const char yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93";
#endif

#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYPATCH 20120115

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)

#define YYPREFIX "yy"

#define YYPURE 0

#line 2 "while.y"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "type.h"

extern int nb_lines ;

extern NODE *mk_node(int type, NODE *n1, NODE *n2);
extern NODE *mk_leaf_int ();
extern NODE *mk_leaf_str();
extern NODE *maj_leaf_str();
#line 39 "while.y"
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union{
  PTR_NODE u_node; 
  char u_char[120];
  int u_int ;
} YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
#line 44 "y.tab.c"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
# ifdef YYPARSE_PARAM_TYPE
#  define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
# else
#  define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
# endif
#else
# define YYPARSE_DECL() yyparse(void)
#endif

/* Parameters sent to lex. */
#ifdef YYLEX_PARAM
# define YYLEX_DECL() yylex(void *YYLEX_PARAM)
# define YYLEX yylex(YYLEX_PARAM)
#else
# define YYLEX_DECL() yylex(void)
# define YYLEX yylex()
#endif

/* Parameters sent to yyerror. */
#ifndef YYERROR_DECL
#define YYERROR_DECL() yyerror(const char *s)
#endif
#ifndef YYERROR_CALL
#define YYERROR_CALL(msg) yyerror(msg)
#endif

extern int YYPARSE_DECL();

#define While 257
#define Skip 258
#define If 259
#define Then 260
#define Else 261
#define Do 262
#define Od 263
#define Assign 264
#define And 265
#define Declare 266
#define Begin 267
#define End 268
#define Integer 269
#define Identifier 270
#define Call 271
#define Proc 272
#define Is 273
#define Var 274
#define Leq 275
#define Geq 276
#define Lexical_error 277
#define YYERRCODE 256
static const short yylhs[] = {                           -1,
    0,    1,    2,    2,    3,    3,    6,    6,    6,    4,
    4,    5,    5,    5,    5,    5,    7,    7,    7,    8,
    8,    9,    9,   10,   10,   10,   11,   11,   12,   12,
   12,   12,   12,   12,   12,
};
static const short yylen[] = {                            2,
    1,    5,    1,    3,    2,    7,    3,    1,    0,    1,
    3,    3,    1,    5,    6,    5,    3,    1,    0,    3,
    1,    3,    1,    3,    1,    1,    3,    1,    3,    3,
    3,    3,    3,    2,    3,
};
static const short yydefred[] = {                         0,
    0,    0,    1,    0,    0,    0,    3,    0,    5,    0,
    0,    0,    0,   13,    0,    0,    0,    0,   10,    4,
    0,    0,   26,   25,    0,    0,    0,    0,   23,    0,
   28,    0,    0,    0,    2,    0,    0,    0,    0,    0,
   34,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,   11,    7,    0,   24,   35,    0,
    0,    0,    0,    0,    0,   22,    0,   27,    0,    0,
    0,    0,    6,   14,    0,   16,    0,   15,   17,
};
static const short yydgoto[] = {                          2,
    3,    6,    7,   18,   19,   22,   71,   27,   28,   29,
   30,   31,
};
static const short yysindex[] = {                      -258,
 -257,    0,    0, -254, -217,  -49,    0,   24,    0, -216,
 -257, -211,  -18,    0,  -18, -198, -200,  -52,    0,    0,
   28,   32,    0,    0,  -18,  -18,  -29,   33,    0, -194,
    0, -209,  -14,   34,    0, -216, -211, -196,  -37,  -40,
    0,  -14,  -14,  -14,  -14,  -14,  -14,  -14, -216,  -18,
 -216,  -14,   35,  -14,    0,    0, -258,    0,    0,   35,
   35,   33,   35,   35,   35,    0, -184,    0, -181,    6,
   40,   -8,    0,    0, -216,    0,  -14,    0,    0,
};
static const short yyrindex[] = {                         0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,   41,    0,    0,    0,    0,    0,    0,    0,    0,
   42,    0,    0,    0,    0,    0,    0,  -41,    0,    0,
    0,    0,    0,    0,    0,    0,   41,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,  -54,   43,    0,    0,    0,    0,    0,   -3,
   -2,  -32,    4,    5,   11,    0,    0,    0,    0,    0,
    0,   44,    0,    0,    0,    0,   43,    0,    0,
};
static const short yygindex[] = {                         0,
   29,    0,   76,    0,   14,   51,   13,   15,   47,   45,
   19,  -13,
};
#define YYTABLESIZE 276
static const short yytable[] = {                         21,
   59,   21,   21,   58,   12,   44,   36,    1,   20,   11,
   20,   20,   41,   44,    4,    8,    5,   21,   21,   21,
   21,   25,   45,   47,   46,   52,   20,   20,   20,   20,
   45,   47,   46,   32,   44,   77,   68,   30,   29,   39,
   13,   14,   15,   40,   31,   32,   58,   53,   44,   55,
   51,   33,    9,   16,   17,   50,   60,   61,   21,   63,
   64,   65,   67,   12,   69,   33,   70,   49,   72,   34,
   50,   37,   38,   54,   48,   26,   57,   44,   74,   75,
   76,    9,    8,   19,   18,   73,   20,   56,   78,   79,
   62,   72,   66,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,   12,    0,   12,    0,
    0,    0,    0,   12,    0,   35,    0,   10,   21,   21,
   21,   21,    0,   21,   50,    0,   21,   20,   20,   20,
   20,    0,   20,   21,   21,   20,    0,   42,   43,    0,
    0,    0,   20,   20,    0,   42,   43,    0,    0,    0,
   23,   24,    0,    0,   23,   24,   30,   29,   30,   29,
    0,   30,   29,   31,   32,   31,   32,    0,   31,   32,
   33,    0,   33,    0,    0,   33,
};
static const short yycheck[] = {                         41,
   41,   43,   44,   41,   59,   43,   59,  266,   41,   59,
   43,   44,   26,   43,  272,  270,  274,   59,   60,   61,
   62,   40,   60,   61,   62,   40,   59,   60,   61,   62,
   60,   61,   62,   15,   43,   44,   50,   41,   41,   25,
  257,  258,  259,   25,   41,   41,   41,   33,   43,   36,
  260,   41,  270,  270,  271,  265,   42,   43,  270,   45,
   46,   47,   49,   40,   51,  264,   52,  262,   54,  270,
  265,   44,   41,   40,   42,   94,  273,   43,  263,  261,
   41,   41,   41,   41,   41,   57,   11,   37,   75,   77,
   44,   77,   48,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  261,   -1,  263,   -1,
   -1,   -1,   -1,  268,   -1,  268,   -1,  267,  260,  261,
  262,  263,   -1,  265,  265,   -1,  268,  260,  261,  262,
  263,   -1,  265,  275,  276,  268,   -1,  275,  276,   -1,
   -1,   -1,  275,  276,   -1,  275,  276,   -1,   -1,   -1,
  269,  270,   -1,   -1,  269,  270,  260,  260,  262,  262,
   -1,  265,  265,  260,  260,  262,  262,   -1,  265,  265,
  260,   -1,  262,   -1,   -1,  265,
};
#define YYFINAL 2
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 277
#if YYDEBUG
static const char *yyname[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,"'('","')'","'*'","'+'","','",0,0,0,0,0,0,0,0,0,0,0,0,0,0,"';'",
"'<'","'='","'>'",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
"'^'",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,"While","Skip","If","Then","Else","Do","Od","Assign","And","Declare",
"Begin","End","Integer","Identifier","Call","Proc","Is","Var","Leq","Geq",
"Lexical_error",
};
static const char *yyrule[] = {
"$accept : program",
"program : bloc",
"bloc : Declare declaration_list Begin instruction_list End",
"declaration_list : declaration",
"declaration_list : declaration_list ';' declaration",
"declaration : Var Identifier",
"declaration : Proc Identifier '(' formal_param_list ')' Is bloc",
"formal_param_list : Identifier ',' formal_param_list",
"formal_param_list : Identifier",
"formal_param_list :",
"instruction_list : instruction",
"instruction_list : instruction_list ';' instruction",
"instruction : Identifier Assign e",
"instruction : Skip",
"instruction : While b Do instruction Od",
"instruction : If b Then instruction Else instruction",
"instruction : Call Identifier '(' effective_param_list ')'",
"effective_param_list : e ',' effective_param_list",
"effective_param_list : e",
"effective_param_list :",
"e : e '+' t",
"e : t",
"t : t '*' f",
"t : f",
"f : '(' e ')'",
"f : Identifier",
"f : Integer",
"b : b And bb",
"b : bb",
"bb : e Geq e",
"bb : e Leq e",
"bb : e '<' e",
"bb : e '>' e",
"bb : e '=' e",
"bb : '^' bb",
"bb : '(' b ')'",

};
#endif

int      yydebug;
int      yynerrs;

int      yyerrflag;
int      yychar;
YYSTYPE  yyval;
YYSTYPE  yylval;

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 500
#define YYMAXDEPTH  500
#endif
#endif

#define YYINITSTACKSIZE 500

typedef struct {
    unsigned stacksize;
    short    *s_base;
    short    *s_mark;
    short    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;
/* variables for the parser stack */
static YYSTACKDATA yystack;
#line 196 "while.y"

#include "lex.yy.c"

#line 307 "y.tab.c"

#if YYDEBUG
#include <stdio.h>		/* needed for printf */
#endif

#include <stdlib.h>	/* needed for malloc, etc */
#include <string.h>	/* needed for memset */

/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    short *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return -1;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = data->s_mark - data->s_base;
    newss = (short *)realloc(data->s_base, newsize * sizeof(*newss));
    if (newss == 0)
        return -1;

    data->s_base = newss;
    data->s_mark = newss + i;

    newvs = (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs));
    if (newvs == 0)
        return -1;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack)) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = YYLEX) < 0) yychar = 0;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
        {
            goto yyoverflow;
        }
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;

    yyerror("syntax error");

    goto yyerrlab;

yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yystack.s_mark]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
                {
                    goto yyoverflow;
                }
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == 0) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
case 1:
#line 66 "while.y"
	{root=yystack.l_mark[0].u_node;YYACCEPT;}
break;
case 2:
#line 71 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(BLOC,yystack.l_mark[-3].u_node,yystack.l_mark[-1].u_node);}
break;
case 3:
#line 76 "while.y"
	{yyval.u_node=yystack.l_mark[0].u_node ;}
break;
case 4:
#line 79 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(SEMI_COLON,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 5:
#line 84 "while.y"
	{yyval.u_node= (PTR_NODE) mk_leaf_str(yystack.l_mark[0].u_char);}
break;
case 6:
#line 87 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(PROC_DECL, mk_leaf_str(yystack.l_mark[-5].u_char), (PTR_NODE) mk_node(PROC ,yystack.l_mark[-3].u_node, yystack.l_mark[0].u_node));}
break;
case 7:
#line 92 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(COMMA,mk_leaf_str(yystack.l_mark[-2].u_char),yystack.l_mark[0].u_node);}
break;
case 8:
#line 95 "while.y"
	{yyval.u_node= mk_leaf_str(yystack.l_mark[0].u_char);}
break;
case 9:
#line 97 "while.y"
	{yyval.u_node = NULL;}
break;
case 10:
#line 102 "while.y"
	{yyval.u_node=yystack.l_mark[0].u_node ;}
break;
case 11:
#line 105 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(SEMI_COLON,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 12:
#line 111 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(ASSIGN,maj_leaf_str(yystack.l_mark[-2].u_char),yystack.l_mark[0].u_node);}
break;
case 13:
#line 114 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(SKIP, NULL,NULL);}
break;
case 14:
#line 117 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(WHILE,yystack.l_mark[-3].u_node,yystack.l_mark[-1].u_node);}
break;
case 15:
#line 120 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(IF,yystack.l_mark[-4].u_node, (PTR_NODE) mk_node(THENELSE,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node));}
break;
case 16:
#line 123 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(CALL,mk_leaf_str(yystack.l_mark[-3].u_char), yystack.l_mark[-1].u_node);}
break;
case 17:
#line 128 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(COMMA,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 18:
#line 131 "while.y"
	{yyval.u_node= yystack.l_mark[0].u_node;}
break;
case 19:
#line 133 "while.y"
	{yyval.u_node = NULL;}
break;
case 20:
#line 138 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(PLUS,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 21:
#line 142 "while.y"
	{yyval.u_node=yystack.l_mark[0].u_node;}
break;
case 22:
#line 147 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(TIMES,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 23:
#line 150 "while.y"
	{yyval.u_node=yystack.l_mark[0].u_node;}
break;
case 24:
#line 155 "while.y"
	{yyval.u_node=yystack.l_mark[-1].u_node;}
break;
case 25:
#line 158 "while.y"
	{yyval.u_node= (PTR_NODE) maj_leaf_str(yystack.l_mark[0].u_char);}
break;
case 26:
#line 161 "while.y"
	{yyval.u_node= (PTR_NODE) mk_leaf_int(yystack.l_mark[0].u_int);}
break;
case 27:
#line 166 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(AND,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 28:
#line 169 "while.y"
	{yyval.u_node=yystack.l_mark[0].u_node;}
break;
case 29:
#line 174 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(SUPEQ,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 30:
#line 177 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(INFEQ,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 31:
#line 180 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(INF,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 32:
#line 183 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(SUP,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 33:
#line 186 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(EGAL,yystack.l_mark[-2].u_node,yystack.l_mark[0].u_node);}
break;
case 34:
#line 190 "while.y"
	{yyval.u_node= (PTR_NODE) mk_node(NOT, yystack.l_mark[0].u_node,NULL);}
break;
case 35:
#line 193 "while.y"
	{yyval.u_node=yystack.l_mark[-1].u_node;}
break;
#line 653 "y.tab.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            if ((yychar = YYLEX) < 0) yychar = 0;
#if YYDEBUG
            if (yydebug)
            {
                yys = 0;
                if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
                if (!yys) yys = "illegal-symbol";
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
    {
        goto yyoverflow;
    }
    *++yystack.s_mark = (short) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    yyerror("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
